//
//  BPUtils.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BPUtils : NSObject

+(NSString*) value:(NSString*) string;

+(UINavigationController*)rootViewController;

+(BOOL) isNullOrEmptyString:(NSString*)string;
@end
