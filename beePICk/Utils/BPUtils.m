//
//  BPUtils.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPUtils.h"
#import "BPAppDelegate.h"
#import "BPNavigationController.h"

@implementation BPUtils

+(NSString *)value:(NSString *)key
{
    return [[NSBundle mainBundle] localizedStringForKey:key value:key table:nil];
}

+(UINavigationController*)rootViewController
{
    BPAppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    UINavigationController* controller = (UINavigationController*)appDelegate.window.rootViewController;
    return controller;
}

+(BOOL) isNullOrEmptyString:(NSString*)string
{
    return ([string isEqual:[NSNull null]] || [[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]);
}

@end
