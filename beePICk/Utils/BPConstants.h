
//
//  Constants.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#pragma mark - beepick constants

#define BEEPICK_FAVORITES_FILE_NAME @"Favorites.txt"

#pragma mark - flickr api constants

// api key
#define BEEPICK_FLICKR_API_KEY @"e44706432363409112c87902f82455bf"

// flickr services and params keys
#define FLICKR_SCHEME @"https"
#define FLICKR_HOST @"api.flickr.com"
#define FLICKR_PATH @"/services/rest/"
#define FLICKR_API_KEY_PARAM @"api_key"

#define FLICKR_METHOD @"method"
#define FLICKR_FORMAT @"format"
#define FLICKR_FORMAT_JSON @"json"

#define FLICKR_RECENT_PHOTOS @"flickr.photos.getRecent"
#define FLICKR_SEARCH_METHOD @"flickr.photos.search"

// SEARCH AND RECENT

#define FLICKR_SEARCH @"text"

// photos extras
#define FLICKR_EXTRAS @"extras"

#define FLICKR_EXTRA_THUMBNAIL_URL @"url_t"
#define FLICKR_EXTRA_SMALL_URL @"url_n"
#define FLICKR_EXTRA_MEDIUM_URL @"url_c"
#define FLICKR_EXTRA_DATE_UPLOAD @"date_upload"
#define FLICKR_EXTRA_OWNER_NAME @"owner_name"
#define FLICKR_EXTRA_FORMAT @"original_format"
#define FLICKR_EXTRA_GEO @"geo"
#define FLICKR_TAGS @"tags"
#define FLICKR_PHOTOS_PER_PAGE @"per_page"
#define FLICKR_PHOTOS_PAGE @"page"
#define FLICKR_FORMAT @"format"
#define FLICKR_NOJSONCALLBACK @"nojsoncallback"
#define FLICKR_PHOTO_ID @"photo_id"

// photos size suffixes

#define FLICKR_SIZE_THUMBNAIL @"t"
#define FLICKR_SIZE_SMALL @"n"
#define FLICKR_SIZE_MEDIUM @"c"


#endif /* Constants_h */
