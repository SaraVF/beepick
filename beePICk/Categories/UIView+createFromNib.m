//
//  UIView+createFromNib.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "UIView+createFromNib.h"

@implementation UIView(createFromNib)

+(id)createFromNib
{
    // I tipically create a nib for each view I have that is named after the class
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
}

@end
