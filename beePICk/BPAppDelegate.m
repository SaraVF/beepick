//
//  AppDelegate.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPAppDelegate.h"
#import "BPImageSearchViewController.h"
#import "BPNavigationController.h"
#import "BPFavoritesImageSearchViewController.h"

@interface BPAppDelegate ()

@end

@implementation BPAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    
    // create tabbar
    BPImageSearchViewController *vcImageSearch = [[BPImageSearchViewController alloc] init];
    BPNavigationController* ncImageSearch = [[BPNavigationController alloc] initWithRootViewController:vcImageSearch];
    BPFavoritesImageSearchViewController* vcFavorites = [[BPFavoritesImageSearchViewController alloc] init];
    BPNavigationController* ncFavorites = [[BPNavigationController alloc] initWithRootViewController:vcFavorites];
    
    UITabBarController* tabBarViewController = [[UITabBarController alloc] init];
    [tabBarViewController setViewControllers:[NSArray arrayWithObjects:ncImageSearch,ncFavorites, nil]];
    
    // create and set main view controller
    
    UINavigationController* mainViewController = [[UINavigationController alloc] initWithRootViewController:tabBarViewController];
    [mainViewController.navigationBar setHidden:YES];
    
    self.window.rootViewController = mainViewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

@end
