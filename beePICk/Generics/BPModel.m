//
//  BPModel.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPModel.h"

@implementation BPModel

-(void) initBase
{
    
}

-(instancetype)init
{
    self = [super init];
    [self initBase];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    
}

@end
