//
//  BPBusinessObject.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BPBusinessObject : NSObject

+(BPBusinessObject*)sharedInstance;

-(void) initBase;

@end
