//
//  BPViewController.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPViewController.h"

@implementation BPViewController

#pragma mark - inits

-(void)initBase
{
    
}

-(instancetype)init
{
    self = [super init];
    [self initBase];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self initBase];
    return self;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    [self initBase];
    return self;
}

@end
