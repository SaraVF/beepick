//
//  BPNavigationControllerViewController.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPNavigationController.h"

@interface BPNavigationController ()

@end

@implementation BPNavigationController

#pragma mark - inits

-(void)initBase
{
    [self.navigationBar setTranslucent:NO];
    [self.navigationBar setHidden:NO];
}

-(instancetype)init
{
    self = [super init];
    [self initBase];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self initBase];
    return self;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    [self initBase];
    return self;
}

#pragma mark - view cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
