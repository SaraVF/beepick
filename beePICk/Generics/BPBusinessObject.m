//
//  BPBusinessObject.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPBusinessObject.h"

static NSMutableDictionary* singletonInstances;

@implementation BPBusinessObject

#pragma mark - singleton management

+(BPBusinessObject*)sharedInstance
{
    BPBusinessObject* instance = nil;
    @synchronized (self) {
        if(!singletonInstances)
        {
            singletonInstances = [NSMutableDictionary dictionary];
        }
        
        instance = [singletonInstances objectForKey:NSStringFromClass(self)];
        if(!instance)
        {
            instance = [[self alloc] init];
            [singletonInstances setObject:instance forKey:NSStringFromClass(self)];
        }
    }
    return instance;
}

#pragma mark - inits

-(void)initBase
{
    
}

-(instancetype)init
{
    self = [super init];
    [self initBase];
    return self;
}

@end
