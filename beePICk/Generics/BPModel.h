//
//  BPModel.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BPModel : NSObject<NSCoding>

-(void) initBase;

@end
