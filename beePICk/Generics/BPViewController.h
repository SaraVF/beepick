//
//  BPViewController.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPViewController : UIViewController

-(void) initBase;

@end
