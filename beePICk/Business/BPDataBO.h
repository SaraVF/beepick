//
//  BPDataBO.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 4/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPBusinessObject.h"

@class BPModel;

@interface BPDataBO : BPBusinessObject
+(BPDataBO*)sharedInstance;

-(void) saveObject:(BPModel*) object toFile:(NSString*)fileName;
-(BPModel*) loadObjectFromFile:(NSString*) fileName;
@end
