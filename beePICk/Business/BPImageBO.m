//
//  BPImageBO.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 2/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPImageBO.h"
#import "BPImageListModel.h"
#import "BPNetworkBO.h"
#import "BPConstants.h"
#import "BPImageModel.h"
#import "BPUtils.h"
#import "BPDataBO.h"

@interface BPImageBO ()
@property (strong, nonatomic) NSURLSessionDataTask* searchTask; // only one at once
@property (strong, nonatomic) BPImageListModel* favoriteImages;
@property (strong, nonatomic) BPImageListModel* searchImages;
@end

@implementation BPImageBO
+(BPImageBO *)sharedInstance
{
    return (BPImageBO*) [super sharedInstance];
}

-(void)initBase
{
    [super initBase];
    _searchImages = [[BPImageListModel alloc] init];
    [self loadFavoritesFromDisk];
}

-(NSURLSessionDataTask*) loadRecentPhotosWithPage:(NSNumber*) page andCompletionHandler:(void (^)(BOOL hasError))completion
{
    return [[BPNetworkBO sharedInstance] loadFlickrRecentPhotosWithExtras:@[FLICKR_EXTRA_OWNER_NAME, FLICKR_EXTRA_FORMAT, FLICKR_EXTRA_DATE_UPLOAD, FLICKR_EXTRA_GEO] photosPerPage:@100 page:page andCompletionHandler:^(BPImageListModel* result, NSError* error){
        if([page integerValue] == 0)
        { // new search
            self.searchImages = result;
        }
        else
        {
            [self.searchImages updateWithImageListModel:result];
        }
        completion(error != nil);
    }];
}

-(NSURLSessionDataTask*) loadSearchPhotosWithSearch:(NSString*)search page:(NSNumber*) page andCompletionHandler:(void (^)(BOOL hasError))completion
{
    return [[BPNetworkBO sharedInstance] loadFlickrPhotosWithSearch:search maxUploadDate:nil extras:@[FLICKR_EXTRA_OWNER_NAME, FLICKR_EXTRA_FORMAT, FLICKR_EXTRA_DATE_UPLOAD, FLICKR_EXTRA_GEO] photosPerPage:@100 page:page andCompletionHandler:^(BPImageListModel *result, NSError *error) {
        if([page integerValue] == 0)
        { // new search
            self.searchImages = result;
        }
        else
        {
            [self.searchImages updateWithImageListModel:result];
        }
        completion(error != nil);
    }];
}

-(void) loadFavoritesFromDisk
{
    _favoriteImages = (BPImageListModel*)[[BPDataBO sharedInstance] loadObjectFromFile:BEEPICK_FAVORITES_FILE_NAME];
    if(!_favoriteImages)
    {
        _favoriteImages = [[BPImageListModel alloc] init];
    }
}

-(void) saveFavoritesToDisk
{
    [[BPDataBO sharedInstance] saveObject:_favoriteImages toFile:BEEPICK_FAVORITES_FILE_NAME];
}

#pragma mark - public methods

#pragma mark - image list

-(NSURLSessionDataTask*)loadNewPageForSearch:(NSString *)search completion:(void (^)(BOOL hasError))completion
{
    if(!search)
    {
        search = @"";
    }
    if([self.lastSearch isEqualToString:search])
    {
        NSUInteger loadedPages = ([self.searchImages.photo count] / [self.searchImages.photosPerPage integerValue]);
        
        if([BPUtils isNullOrEmptyString:search])
        {
            return [self loadRecentPhotosWithPage:[NSNumber numberWithInteger:loadedPages+1] andCompletionHandler:completion];
        }
        else
        {
            return [self loadSearchPhotosWithSearch:search page:[NSNumber numberWithInteger:loadedPages+1] andCompletionHandler:completion];
        }
    }
    else
    {
        self.lastSearch = [NSString stringWithString:search];
        
        if([BPUtils isNullOrEmptyString:search])
        {
            // load the first 100 last flickr images
            return [self loadRecentPhotosWithPage:@0 andCompletionHandler:completion];
        }
        else
        {
            return [self loadSearchPhotosWithSearch:search page:@0 andCompletionHandler:completion];
        }
    }
}

-(BPImageListModel *)searchImages
{
    return _searchImages;
}


-(BPImageListModel *)favoriteImages
{
    return _favoriteImages;
}

-(void)addFavorite:(BPImageModel *)favorite
{
    [self addFavorite:favorite saveToDisk:YES];
}

-(void)addFavorite:(BPImageModel *)favorite saveToDisk:(BOOL)save
{
    if(!favorite)
        return;
    
    if(![self imageIsFavorite:favorite])
    {
        [_favoriteImages.photo addObject:favorite];
        if(save)
        {
            [self saveFavoritesToDisk];
        }
    }
}

-(void)removeFavorite:(BPImageModel *)favorite
{
    [self removeFavorite:favorite saveToDisk:YES];
}

-(void)removeFavorite:(BPImageModel *)favorite saveToDisk:(BOOL)save
{
    if(!favorite)
        return;
    
    BPImageModel* favoriteEqual;
    for(NSUInteger i = 0; i < [_favoriteImages.photo count] && !favoriteEqual; ++i)
    {
        BPImageModel* image = [_favoriteImages.photo objectAtIndex:i];
        if([image.imageId isEqualToString:favorite.imageId])
        {
            favoriteEqual = image;
        }
    }
    
    if(favoriteEqual)
    {
        [_favoriteImages.photo removeObject:favoriteEqual];
        if(save)
        {
            [self saveFavoritesToDisk];
        }
    }
}

-(void)clearFavorites
{
    _favoriteImages.photo = [NSMutableArray<BPImageModel*> array];
    [self saveFavoritesToDisk];
}

-(BOOL) imageIsFavorite:(BPImageModel*)image
{
    if(!image)
        return NO;
    
    BOOL existsFavorite = NO;
    for(NSUInteger i = 0; i < [_favoriteImages.photo count] && !existsFavorite; ++i)
    {
        BPImageModel* favorite = [_favoriteImages.photo objectAtIndex:i];
        if([favorite.imageId isEqualToString:image.imageId])
        {
            existsFavorite = YES;
        }
    }
    
    return existsFavorite;
}

-(void)switchFromFavorites:(BPImageModel *)image
{
    if([self imageIsFavorite:image])
    {
        [self removeFavorite:image saveToDisk:NO];
    }
    else
    {
        [self addFavorite:image saveToDisk:NO];
    }
    [self saveFavoritesToDisk];
}

@end
