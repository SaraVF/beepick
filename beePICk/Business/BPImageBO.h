//
//  BPImageBO.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 2/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPBusinessObject.h"
#import <UIKit/UIKit.h>

@class BPImageListModel;
@class BPImageModel;

@interface BPImageBO : BPBusinessObject
+(BPImageBO*)sharedInstance;

@property (strong, nonatomic) NSString* lastSearch;

-(NSURLSessionDataTask*)loadNewPageForSearch:(NSString *)search completion:(void (^)(BOOL hasError))completion;

-(BPImageListModel*) searchImages;
-(BPImageListModel*) favoriteImages;

-(void) addFavorite:(BPImageModel*) favorite;
-(void) addFavorite:(BPImageModel *)favorite saveToDisk:(BOOL) save;
-(void) removeFavorite:(BPImageModel*) favorite;
-(void) removeFavorite:(BPImageModel *)favorite saveToDisk:(BOOL) save;
-(void) clearFavorites;
-(BOOL) imageIsFavorite:(BPImageModel*)image;
-(void) switchFromFavorites:(BPImageModel*) image;

@end
