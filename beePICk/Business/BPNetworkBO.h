//
//  BPNetworkBO.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPBusinessObject.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class BPImageListModel;

@interface BPNetworkBO : BPBusinessObject

+(BPNetworkBO*) sharedInstance;

-(NSURLSessionDataTask*)loadFlickrRecentPhotosWithExtras:(NSArray *)extras photosPerPage:(NSNumber *)photosPerPage page:(NSNumber *)page andCompletionHandler:(void (^)(BPImageListModel* result,NSError* error))completion;

-(NSURLSessionDataTask*)loadFlickrPhotosWithSearch:(NSString*) search maxUploadDate:(NSNumber*) date extras:(NSArray*) extras photosPerPage:(NSNumber *)photosPerPage page:(NSNumber *)page andCompletionHandler:(void (^)(BPImageListModel* result,NSError* error))completion;

@end
