//
//  BPDataBO.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 4/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPDataBO.h"
#import "BPModel.h"

@implementation BPDataBO

+(BPDataBO*)sharedInstance
{
    return (BPDataBO*) [super sharedInstance];
}

-(NSString*) beePickPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"/BeePick"];
    
    
    NSError* error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
        [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:NO attributes:nil error:&error];
    
    if(!error)
    {
        return filePath;
    }
    else
    {
        return nil;
    }
}


#pragma mark - public functions

-(BPModel*)loadObjectFromFile:(NSString *)fileName
{
    NSString* filePath = [self beePickPath];
    if(filePath)
    {
        NSString* dataPath = [filePath stringByAppendingPathComponent:fileName];
        return [NSKeyedUnarchiver unarchiveObjectWithFile:dataPath];
    }
    return nil;
}

-(void)saveObject:(BPModel *)object toFile:(NSString *)fileName
{
    NSString* filePath = [self beePickPath];
    if(filePath)
    {
        NSString* dataPath = [filePath stringByAppendingPathComponent:fileName];
        [NSKeyedArchiver archiveRootObject:object toFile:dataPath];
    }
}

@end
