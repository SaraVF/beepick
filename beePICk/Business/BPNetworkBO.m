//
//  BPNetworkBO.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPNetworkBO.h"
#import <UIKit/UIKit.h>
#import "BPUtils.h"
#import "BPConstants.h"
#import "BPImageListModel.h"

@interface BPNetworkBO ()
@property (strong, nonatomic) NSURLSession* urlSession;
@end

@implementation BPNetworkBO

-(void)initBase
{
    [super initBase];
    
    _urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
}

#pragma mark - private functions

-(NSURLSessionDataTask*)dataFromServer:(NSURL*)url withCompletionHandler:(void(^)(NSData* data, NSURLResponse* response, NSError* error)) completion
{
    if(!url || [url isEqual:[NSNull null]])
        return nil;
    
    NSURLSessionDataTask* dataTask = [_urlSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(data,response,error);
        });
    }];
    [dataTask resume];
    return dataTask;
}

-(NSMutableArray*) flickrBaseQuery
{
    NSMutableArray* queryArray = [NSMutableArray array];
    [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_API_KEY_PARAM,BEEPICK_FLICKR_API_KEY]];
    [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_NOJSONCALLBACK,[[NSNumber numberWithBool:YES] stringValue]]];
    [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_FORMAT,FLICKR_FORMAT_JSON]];
    
    return queryArray;
}

#pragma mark - public functions

-(NSURLSessionDataTask*)loadFlickrRecentPhotosWithExtras:(NSArray *)extras photosPerPage:(NSNumber *)photosPerPage page:(NSNumber *)page andCompletionHandler:(void (^)(BPImageListModel* result,NSError* error))completion
{
    NSMutableArray* queryArray = [self flickrBaseQuery];
    [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_METHOD, FLICKR_RECENT_PHOTOS]];
    
    if(![BPUtils isNullOrEmptyString:[extras componentsJoinedByString:@","]])
        [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_EXTRAS,[extras componentsJoinedByString:@","]]];
    if(![BPUtils isNullOrEmptyString:[page stringValue]])
        [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_PHOTOS_PAGE,[page stringValue]]];
    if(![BPUtils isNullOrEmptyString:[photosPerPage stringValue]])
        [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_PHOTOS_PER_PAGE,[photosPerPage stringValue]]];
    
    
    NSURLComponents* urlComponents = [[NSURLComponents alloc] init];
    [urlComponents setScheme:FLICKR_SCHEME];
    [urlComponents setHost:FLICKR_HOST];
    [urlComponents setPath:FLICKR_PATH];
    [urlComponents setQuery:[queryArray componentsJoinedByString:@"&"]];
    
    return [self dataFromServer:[urlComponents URL] withCompletionHandler:^(NSData* data, NSURLResponse* response, NSError* error){
        BPImageListModel* list;
        if(data != nil)
        {
            NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            list = [BPImageListModel createFromDictionary:dict];
        }
        completion(list, error);
    }];
}

-(NSURLSessionDataTask*)loadFlickrPhotosWithSearch:(NSString *)search maxUploadDate:(NSNumber*)date extras:(NSArray *)extras photosPerPage:(NSNumber *)photosPerPage page:(NSNumber *)page andCompletionHandler:(void (^)(BPImageListModel *, NSError *))completion
{
    NSMutableArray* queryArray = [self flickrBaseQuery];
    [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_METHOD, FLICKR_SEARCH_METHOD]];
    
    if(![BPUtils isNullOrEmptyString:[extras componentsJoinedByString:@","]])
        [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_EXTRAS,[extras componentsJoinedByString:@","]]];
    if(![BPUtils isNullOrEmptyString:[page stringValue]])
        [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_PHOTOS_PAGE,[page stringValue]]];
    if(![BPUtils isNullOrEmptyString:[photosPerPage stringValue]])
        [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_PHOTOS_PER_PAGE,[photosPerPage stringValue]]];
    if(![BPUtils isNullOrEmptyString:search])
        [queryArray addObject:[NSString stringWithFormat:@"%@=%@",FLICKR_SEARCH,search]];
    
    if(date && ![date isEqual:[NSNull null]])
        [queryArray addObject:[NSString stringWithFormat:@"%@=%ld",FLICKR_SEARCH,[date longValue]]];
    
    NSURLComponents* urlComponents = [[NSURLComponents alloc] init];
    [urlComponents setScheme:FLICKR_SCHEME];
    [urlComponents setHost:FLICKR_HOST];
    [urlComponents setPath:FLICKR_PATH];
    [urlComponents setQuery:[queryArray componentsJoinedByString:@"&"]];
    
    return [self dataFromServer:[urlComponents URL] withCompletionHandler:^(NSData* data, NSURLResponse* response, NSError* error){
        BPImageListModel* list;
        if(data != nil)
        {
            NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            list = [BPImageListModel createFromDictionary:dict];
        }
        completion(list, error);
    }];
}


#pragma mark - singleton

+(BPNetworkBO*) sharedInstance
{
    return (BPNetworkBO*) [super sharedInstance];
}

@end
