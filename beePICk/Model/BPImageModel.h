//
//  BPImageModel.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 2/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPModel.h"

@interface BPImageModel : BPModel

@property (strong, nonatomic) NSString* imageTitle;
@property (strong, nonatomic) NSString* ownerName;
@property (strong, nonatomic) NSString* imageId;
@property (strong, nonatomic) NSString* serverId;
@property (strong, nonatomic) NSNumber* farmId;
@property (strong, nonatomic) NSString* secret;
@property (strong, nonatomic) NSString* thumbNailImageUrl;
@property (strong, nonatomic) NSString* mediumImageUrl;
@property (strong, nonatomic) NSString* smallImageUrl;
@property (strong, nonatomic) NSString* format;
@property (strong, nonatomic) NSDate* dateUpload;
@property (strong, nonatomic) NSNumber* latitude;
@property (strong, nonatomic) NSNumber* longitude;

+(BPImageModel*) createFromDictionary:(NSDictionary*) dict;

-(NSURL*) imageUrlForSize:(NSString*) size;

@end
