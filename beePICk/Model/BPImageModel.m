//
//  BPImageModel.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 2/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPImageModel.h"
#import "BPConstants.h"
#import "BPImageBO.h"

@implementation BPImageModel

-(void)initBase
{
    [super initBase];
}

+(BPImageModel *)createFromDictionary:(NSDictionary *)dict
{
    BPImageModel* result = [[BPImageModel alloc] init];
    result.imageTitle = [dict objectForKey:@"title"];
    result.imageId = [dict objectForKey:@"id"];
    result.serverId = [dict objectForKey:@"server"];
    result.secret = [dict objectForKey:@"secret"];
    result.farmId = [dict objectForKey:@"farm"];
    result.ownerName = [dict objectForKey:@"ownername"];
    result.format = [dict objectForKey:@"originalformat"];
    NSNumber* dateUpload = [dict objectForKey:@"dateupload"];
    result.dateUpload = [NSDate dateWithTimeIntervalSince1970:[dateUpload doubleValue]];
    result.latitude = [dict objectForKey:@"latitude"];
    result.longitude = [dict objectForKey:@"longitude"];
    
    return result;
}

#pragma mark - public functions

-(NSURL *)imageUrlForSize:(NSString *)size
{
    NSString* urlString;
    if([size isEqualToString:FLICKR_SIZE_THUMBNAIL])
    {
        urlString = self.thumbNailImageUrl;
    }
    else if([size isEqualToString:FLICKR_SIZE_SMALL])
    {
        urlString = self.smallImageUrl;
    }
    else if([size isEqualToString:FLICKR_SIZE_MEDIUM])
    {
        urlString = self.mediumImageUrl;
    }
    if(!urlString)
    { // just in case
        urlString = [NSString stringWithFormat:@"https://farm%ld.staticflickr.com/%@/%@_%@_%@.%@", [self.farmId integerValue], self.serverId, self.imageId, self.secret, size, (self.format?self.format:@"jpg")];
    }
    NSURL *url = [NSURL URLWithString:urlString];
    return url;
}


#pragma mark - save/load from disk

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    
    [aCoder encodeObject:_imageTitle forKey:@"imageTitle"];
    [aCoder encodeObject:_ownerName forKey:@"ownerName"];
    [aCoder encodeObject:_imageId forKey:@"imageId"];
    [aCoder encodeObject:_serverId forKey:@"serverId"];
    [aCoder encodeObject:_farmId forKey:@"farmId"];
    [aCoder encodeObject:_secret forKey:@"secret"];
    [aCoder encodeObject:_thumbNailImageUrl forKey:@"thumbnailImageUrl"];
    [aCoder encodeObject:_mediumImageUrl forKey:@"mediumImageUrl"];
    [aCoder encodeObject:_smallImageUrl forKey:@"smallImageUrl"];
    [aCoder encodeObject:_format forKey:@"format"];
    [aCoder encodeObject:_dateUpload forKey:@"dateUpload"];
    [aCoder encodeObject:_latitude forKey:@"latitude"];
    [aCoder encodeObject:_longitude forKey:@"longitude"];
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    self.imageTitle = [aDecoder decodeObjectForKey:@"imageTitle"];
    self.ownerName = [aDecoder decodeObjectForKey:@"ownerName"];
    self.imageId = [aDecoder decodeObjectForKey:@"imageId"];
    self.serverId = [aDecoder decodeObjectForKey:@"serverId"];
    self.farmId = [aDecoder decodeObjectForKey:@"farmId"];
    self.secret = [aDecoder decodeObjectForKey:@"secret"];
    self.thumbNailImageUrl = [aDecoder decodeObjectForKey:@"thumbnailImageUrl"];
    self.mediumImageUrl = [aDecoder decodeObjectForKey:@"mediumImageUrl"];
    self.smallImageUrl = [aDecoder decodeObjectForKey:@"smallImageUrl"];
    self.format = [aDecoder decodeObjectForKey:@"format"];
    self.dateUpload = [aDecoder decodeObjectForKey:@"dateUpload"];
    self.latitude = [aDecoder decodeObjectForKey:@"latitude"];
    self.longitude = [aDecoder decodeObjectForKey:@"longitude"];
    
    return self;
}

@end
