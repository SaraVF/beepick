//
//  BPImageListModel.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPImageListModel.h"
#import "BPImageModel.h"

@implementation BPImageListModel

-(void)initBase
{
    [super initBase];
    
    _photo = [NSMutableArray<BPImageModel*> array];
}

+(BPImageListModel *)createFromDictionary:(NSDictionary *)dict
{
    BPImageListModel* result = [[BPImageListModel alloc] init];
    NSDictionary* photos = [dict objectForKey:@"photos"];
    
    result.totalPhotos = [photos objectForKey:@"total"];
    result.photosPerPage = [photos objectForKey:@"perpage"];
    result.page = [photos objectForKey:@"page"];
    
    NSArray* photo = [photos objectForKey:@"photo"];
    
    for(NSDictionary* photoDict in photo)
    {
        BPImageModel* newPhoto = [BPImageModel createFromDictionary:photoDict];
        if(newPhoto)
        {
            [result.photo addObject:newPhoto];
        }
    }
    
    return result;
}

-(void)updateWithImageListModel:(BPImageListModel *)newList
{
    if([newList.photosPerPage integerValue] != [_photosPerPage integerValue]) // photos per page must be equal
        return;
    NSUInteger loadedPages = ([_photo count] / [_photosPerPage integerValue]);
    if([newList.page integerValue] > (loadedPages + 1)) // pages can't be empty
        return;
    
    if([newList.page integerValue] <= [_page integerValue])
    { // modify page
        NSUInteger firstIndex = ([_page integerValue] - 1)*[_photosPerPage integerValue];
        NSUInteger i;
        for(i = 0; i < [_photosPerPage integerValue] && i + firstIndex < [_photo count]; ++i)
        {
            [_photo replaceObjectAtIndex:(firstIndex + i) withObject:[newList.photo objectAtIndex:i]];
        }
        
        if(i + firstIndex < [_photo count])
        {
            while (i < [newList.photo count])
            {
                [_photo addObject:[newList.photo objectAtIndex:i]];
            }
        }
    }
    else
    {
        for(NSUInteger i = 0; i < [newList.photo count]; ++i)
        {
            [_photo addObject:[newList.photo objectAtIndex:i]];
        }
    }
}

#pragma mark - save/load from disk

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:_photo forKey:@"photo"];
    [aCoder encodeObject:_page forKey:@"page"];
    [aCoder encodeObject:_photosPerPage forKey:@"photosPerPage"];
    [aCoder encodeObject:_totalPhotos forKey:@"totalPhotos"];
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    self.photo = [aDecoder decodeObjectForKey:@"photo"];
    self.page = [aDecoder decodeObjectForKey:@"page"];
    self.photosPerPage = [aDecoder decodeObjectForKey:@"photosPerPage"];
    self.totalPhotos = [aDecoder decodeObjectForKey:@"totalPhotos"];
    
    return self;
}

@end
