//
//  BPImageListModel.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPModel.h"

@class BPImageModel;

@interface BPImageListModel : BPModel

@property (strong, nonatomic) NSMutableArray<BPImageModel*>* photo;
@property (strong, nonatomic) NSNumber* page;
@property (strong, nonatomic) NSNumber* photosPerPage;
@property (strong, nonatomic) NSNumber* totalPhotos;

+(BPImageListModel *)createFromDictionary:(NSDictionary *)dict;

-(void) updateWithImageListModel:(BPImageListModel*) newList;

@end
