//
//  BPImageDetailViewController.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 3/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPImageDetailViewController.h"
#import "BPImageBO.h"
#import "BPImageModel.h"

@interface BPImageDetailViewController ()
@property (strong, nonatomic) BPImageDetailView* vImageDetail;
@end

@implementation BPImageDetailViewController

-(void)loadView
{
    [super loadView];
    
    _vImageDetail = [BPImageDetailView createFromNib];
    _vImageDetail.delegate = self;
    
    [_vImageDetail setImageModelDetails:_imageModel];
    
    self.view = _vImageDetail;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone; // don't cover collectionview with tabbar
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _vImageDetail.imageModel = _imageModel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setImageModel:(BPImageModel *)imageModel
{
    _imageModel = imageModel;
    
    self.title = imageModel.imageTitle;
    
    _vImageDetail.imageModel = _imageModel;
}

#pragma mark - view delegate

-(void)imageDetailViewDoneDidPress
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imageDetailViewFavoriteDidPress
{
    [[BPImageBO sharedInstance] switchFromFavorites:_imageModel];
    [_vImageDetail setImageModelDetails:_imageModel];
}

-(BOOL)imageDetailViewIsFavorite
{
    return [[BPImageBO sharedInstance] imageIsFavorite:_imageModel];
}

@end
