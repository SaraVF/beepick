//
//  BPImageDetailViewController.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 3/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPViewController.h"
#import "BPImageDetailView.h"

@class BPImageModel;

@interface BPImageDetailViewController : BPViewController<BPImageDetailViewDelegate>
@property (strong, nonatomic) BPImageModel* imageModel;
@end
