//
//  BPImageDetailView.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 3/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPView.h"
#import "UIView+createFromNib.h"
#import <MapKit/MapKit.h>

@class BPImageModel;

@protocol BPImageDetailViewDelegate
-(void) imageDetailViewDoneDidPress;
-(void) imageDetailViewFavoriteDidPress;
-(BOOL) imageDetailViewIsFavorite;
@end

@interface BPImageDetailView : BPView

@property (strong, nonatomic) IBOutlet UIImageView* ivImage;
@property (strong, nonatomic) IBOutlet UILabel* lbTitle;
@property (strong, nonatomic) IBOutlet UILabel* lbOwnerName;
@property (strong, nonatomic) IBOutlet UILabel* lbDateUploaded;
@property (strong, nonatomic) IBOutlet UIButton* btDone;
@property (strong, nonatomic) IBOutlet UIButton* btFavorite;
@property (strong, nonatomic) IBOutlet MKMapView* mapView;


@property (strong, nonatomic) BPImageModel* imageModel; // as we are using all imageModel params we will skip creating a view model

-(void) setImageModelDetails:(BPImageModel*) imageModel;

@property (weak, nonatomic) id<BPImageDetailViewDelegate> delegate;


@end
