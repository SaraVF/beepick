//
//  BPImageDetailView.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 3/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPImageDetailView.h"
#import "BPImageModel.h"
#import "Haneke.h"
#import "BPConstants.h"
#import "BPUtils.h"

@interface BPImageDetailView ()
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* cMapTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* cMapBottom;

@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSMutableArray* detailConstraints;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray* fullScreenConstraints;

@property (nonatomic) BOOL isFullScreen;
@end

@implementation BPImageDetailView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    UITapGestureRecognizer* imageTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageDidPress:)];
    imageTapRecognizer.numberOfTapsRequired = 1;
    [self.ivImage addGestureRecognizer:imageTapRecognizer];
    self.ivImage.userInteractionEnabled = YES;
    self.isFullScreen = NO;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}

-(void)setImageModel:(BPImageModel *)imageModel
{
    _imageModel = imageModel;
    
    [self setImageModelDetails:_imageModel];
    
    [_ivImage hnk_setImageFromURL:[_imageModel imageUrlForSize:FLICKR_SIZE_MEDIUM] placeholder:[UIImage imageNamed:@"Bee1.png"] success:^(UIImage *image) {
        self.ivImage.image = image;
//        _cImageHeight.constant = self.ivImage.frame.size.width * image.size.height / image.size.width;
//        [UIView animateWithDuration:0.6 animations:^{
//            [self layoutIfNeeded];
//        }];
    } failure:^(NSError *error) {
        
    }];
    
}

-(void) setImageModelDetails:(BPImageModel *)imageModel
{
    if(!imageModel)
        return;
    _lbTitle.text = imageModel.imageTitle;
    _lbOwnerName.text = imageModel.ownerName;
    _lbDateUploaded.text = [NSDateFormatter localizedStringFromDate:imageModel.dateUpload
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:NSDateFormatterMediumStyle];
    
    [_btFavorite setImage:[UIImage imageNamed:([_delegate imageDetailViewIsFavorite]?@"favorite_yes.png":@"favorite_no.png")] forState:UIControlStateNormal];
    

    if([imageModel.latitude floatValue] != 0.f || [imageModel.longitude floatValue] != 0.f)
    {
        CLLocationCoordinate2D zoomLocation;
        zoomLocation.latitude = [imageModel.latitude floatValue];
        zoomLocation.longitude= [imageModel.longitude floatValue];
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 12*1000*1000, 12*1000*1000);
        [_mapView setRegion:viewRegion animated:YES];
        
        MKPointAnnotation* annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = zoomLocation;
        [self.mapView addAnnotation:annotation];
    }
    else
    {
        // instead of removing from xib, we should add it only if needed
        [self.mapView removeFromSuperview];
        [self removeConstraint:self.cMapTop];
        [self removeConstraint:self.cMapBottom];
        
        self.mapView = nil;
        
    }
}

-(IBAction)buttonDoneDidPress:(id)sender
{
    [_delegate imageDetailViewDoneDidPress];
}

-(IBAction)buttonFavoriteDidPress:(id)sender
{
    [_delegate imageDetailViewFavoriteDidPress];
}

-(void)imageDidPress:(id)sender
{
    [self setIsFullScreen:!self.isFullScreen animated:YES];
}

-(void) setIsFullScreen:(BOOL)isFullScreen animated:(BOOL)animate
{
    [self setIsFullScreen:isFullScreen];
    
    if(animate)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self layoutIfNeeded];
        }];
    }
}

-(void)setIsFullScreen:(BOOL)isFullScreen
{
    _isFullScreen = isFullScreen;
    
    if(isFullScreen)
    {
        for(NSLayoutConstraint* constraint in _detailConstraints)
        {
            if(constraint != self.cMapBottom || self.mapView)
            {
                constraint.active = NO;
            }
        }
        for(NSLayoutConstraint* constraint in _fullScreenConstraints)
        {
            constraint.active = YES;
        }
    }
    else
    {
        for(NSLayoutConstraint* constraint in _fullScreenConstraints)
        {
            constraint.active = NO;
        }
        for(NSLayoutConstraint* constraint in _detailConstraints)
        {
            if(constraint != self.cMapBottom || self.mapView)
            {
                constraint.active = YES;
            }
        }
    }}

@end
