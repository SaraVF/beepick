//
//  BPFavoritesImageSearchViewController.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPFavoritesImageSearchViewController.h"
#import "BPUtils.h"
#import "BPFavoritesView.h"
#import "BPImageBO.h"
#import "BPImageModel.h"
#import "BPImageListModel.h"

@interface BPFavoritesImageSearchViewController ()
@property (strong, nonatomic) BPFavoritesView* vFavorites;
@end

@implementation BPFavoritesImageSearchViewController

-(void)initBase
{
    [super initBase];
    
    self.title = [BPUtils value:@"Favorites_title"];
    self.tabBarItem.image = [UIImage imageNamed:@"favoriteIcon.png"];
}

-(void)loadView
{
    _vFavorites = [BPFavoritesView createFromNib];
    self.view = _vFavorites;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - inherited methods

-(BPImageSearchView *)vImageSearch
{
    return _vFavorites;
}

-(BOOL)hasMorePhotosToLoad
{
    return NO;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    // will never happen, but just in case
}

-(void) loadNewPageForSearch:(NSString*) strSearch showActivityIndicator:(BOOL) showAI replaceImages:(BOOL)replaceImages
{
    
}

-(void)imageCellDidPressChangeFavoriteAtIndexRow:(NSUInteger)indexRow
{
    BPImageModel* cellImageModel = (BPImageModel*)[self.images.photo objectAtIndex:indexRow];
    
    [[BPImageBO sharedInstance] removeFavorite:cellImageModel];
    
    [self.vImageSearch reloadData];
}

-(BPImageListModel *)images
{
    return [[BPImageBO sharedInstance] favoriteImages];
}

-(BOOL) mustShowActivityIndicator
{
    return NO;
}

-(void) refreshControlAction
{
    
}
-(BOOL) hasRefreshControl
{
    return NO;
}

@end
