//
//  BPImageCollectionViewCell.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 2/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPImageCollectionViewCell.h"
#import "UIImageView+Haneke.h"

@implementation BPImageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 1.f;
    self.isFavorite = NO;
    _rowIndex = -1;
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    
    [self.ivImage hnk_cancelSetImage];
    self.ivImage.image = nil;
}

-(void)setIsFavorite:(BOOL)isFavorite
{
    _isFavorite = isFavorite;
    
    if(_isFavorite)
    {
        self.layer.borderColor = [UIColor orangeColor].CGColor;
        [self.btFavorite setImage:[UIImage imageNamed:@"favorite_yes.png"] forState:UIControlStateNormal];
    }
    else
    {
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        [self.btFavorite setImage:[UIImage imageNamed:@"favorite_no.png"] forState:UIControlStateNormal];
    }
}

-(IBAction)buttonFavoriteDidPress:(id)sender
{
    [_delegate imageCellDidPressChangeFavoriteAtIndexRow:_rowIndex];
}

@end
