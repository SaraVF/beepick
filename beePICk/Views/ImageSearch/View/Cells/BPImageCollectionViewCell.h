//
//  BPImageCollectionViewCell.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 2/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+createFromNib.h"

@protocol BPImageCollectionViewCellDelegate

-(void) imageCellDidPressChangeFavoriteAtIndexRow:(NSUInteger)indexRow;

@end

@interface BPImageCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView* ivImage;
@property (strong, nonatomic) IBOutlet UIButton* btFavorite;
@property (nonatomic) BOOL isFavorite;

@property (nonatomic) NSUInteger rowIndex;

@property (weak, nonatomic) id<BPImageCollectionViewCellDelegate> delegate;
@end
