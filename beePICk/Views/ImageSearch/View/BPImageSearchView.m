//
//  BPImageSearchView.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPImageSearchView.h"

@implementation BPImageSearchView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self showActivityIndicator:NO];
}

-(void) reloadData
{
    [self.cvImages reloadData];
}

-(void) updateView
{
    [self.cvImages performBatchUpdates:^{
        // resize images
        [self.cvImages setNeedsLayout];
    } completion:^(BOOL finished) {
        
    }];
}

-(void) showActivityIndicator:(BOOL) show
{
    self.cActivityIndicatorHeight.constant = (show?60:0);
    self.cvImages.contentInset = UIEdgeInsetsMake(self.cvImages.contentInset.top, self.cvImages.contentInset.left, show?60:0, self.cvImages.contentInset.right);
    
    [UIView animateWithDuration:0.4 animations:^{
        [self layoutIfNeeded];
    }];
    
}

@end
