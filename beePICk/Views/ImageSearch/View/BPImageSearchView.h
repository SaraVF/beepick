//
//  BPImageSearchView.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPView.h"
#import "UIView+createFromNib.h"

@interface BPImageSearchView : BPView

-(void) reloadData;
-(void) updateView;
-(void) showActivityIndicator:(BOOL) show;

@property (strong, nonatomic) IBOutlet UISearchBar* sbSearch;
@property (strong, nonatomic) IBOutlet UICollectionView* cvImages;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint* cActivityIndicatorHeight;

@end
