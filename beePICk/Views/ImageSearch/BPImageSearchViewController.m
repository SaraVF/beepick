//
//  BPImageSearchViewController.m
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPImageSearchViewController.h"
#import "BPImageSearchView.h"
#import "BPUtils.h"
#import "BPImageBO.h"
#import "BPImageListModel.h"
#import "BPImageModel.h"
#import "BPConstants.h"
#import "UIImageView+Haneke.h"
#import "Haneke.h"
#import "BPImageDetailViewController.h"

@interface BPImageSearchViewController ()
@property (strong, nonatomic) BPImageSearchView* vImageSearch;

@property (strong, nonatomic) NSURLSessionDataTask* searchTask;

@property (strong, nonatomic) NSString* searchString;

@property (strong, nonatomic) UIRefreshControl* refreshControl;

// collection view
@property (nonatomic) NSUInteger cellSize;
@property (nonatomic) NSUInteger maxNumberOfCellsInScreen;
@property (nonatomic) NSUInteger offsetForInfiniteScroll; // if it is 0 we can't scroll up
@end

@implementation BPImageSearchViewController

static const NSUInteger kCollectionColums = 3;
static const NSUInteger kInterItemSpacing = 10;
static const NSUInteger kInterRowSpacing = 10;

-(void)initBase
{
    [super initBase];
    
    self.title = [BPUtils value:@"ImageSearch_title"];
    _cellSize = 75;
    _offsetForInfiniteScroll = 0;
    
    self.tabBarItem.image = [UIImage imageNamed:@"BeeIcon.png"];
}

-(void)loadView
{
    _vImageSearch = [BPImageSearchView createFromNib];
    self.view = _vImageSearch;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vImageSearch.sbSearch.delegate = self;
    self.vImageSearch.cvImages.dataSource = self;
    self.vImageSearch.cvImages.delegate = self;
    
    [self.vImageSearch.cvImages registerNib:[UINib nibWithNibName:@"BPImageCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"flickrImageSearchCell"];
    
    [self loadNewPageForSearch:@"" showActivityIndicator:YES replaceImages:NO];
    
    self.edgesForExtendedLayout = UIRectEdgeNone; // don't cover collectionview with tabbar
    
    [self.vImageSearch.cvImages setAllowsMultipleSelection:YES];
    
    if([self hasRefreshControl])
    {
        // refresh control
        self.refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl.tintColor = [UIColor whiteColor];
        [self.refreshControl addTarget:self action:@selector(refreshControlAction) forControlEvents:UIControlEventValueChanged];
        [self.vImageSearch.cvImages addSubview:self.refreshControl];
        self.vImageSearch.cvImages.alwaysBounceVertical = YES;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.vImageSearch reloadData];
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    _cellSize = 75;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // update cell size
    
    if(kCollectionColums < 1)
    {
        _cellSize = 75;
    }
    CGFloat interItemSpacing = (kCollectionColums -1)*kInterItemSpacing;
    CGFloat collectionWidth = MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    CGFloat collectionRightInset = self.vImageSearch.cvImages.contentInset.right;
    CGFloat collectionLeftInset = self.vImageSearch.cvImages.contentInset.left;
    
    CGFloat usableWidth = collectionWidth - interItemSpacing - collectionRightInset - collectionLeftInset;
    
    _cellSize = usableWidth/ kCollectionColums;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setImages:(BPImageListModel *)images
{
    _images = images;
}

-(void) cancelPreviousSearch
{
    if(self.searchTask)
    {
        [self.searchTask cancel];
        self.searchTask = nil;
    }
}

-(void) lastImageDidShow
{
    if([self hasMorePhotosToLoad])
    { // there are more objects to load
        [self loadNewPageForSearch:self.searchString showActivityIndicator:YES replaceImages:NO];
    }
}

-(NSUInteger) realRowFromInfiniteScrollRow:(NSUInteger) row
{
    NSUInteger realRow = (row + _offsetForInfiniteScroll)% [self.images.photo count];
    return realRow;
}

-(NSUInteger) maximumNumberOfDisplayedCellsInScreen
{
    CGRect screenRect = self.vImageSearch.cvImages.frame;
    CGFloat screenHeight = screenRect.size.height;
    
    CGFloat rowNeededHeight = kInterRowSpacing + _cellSize;
    
    return kCollectionColums*(2 + (NSInteger)(screenHeight/rowNeededHeight));
}

#pragma mark - methods to be inherited

-(BPImageSearchView *)vImageSearch
{
    return _vImageSearch;
}

-(BOOL) hasMorePhotosToLoad
{
    return [[self.images totalPhotos] integerValue] > [self realCollectionView:self.vImageSearch.cvImages numberOfItemsInSection:0];
}

-(void) loadNewPageForSearch:(NSString*) strSearch showActivityIndicator:(BOOL) showAI replaceImages:(BOOL)replaceImages
{
    if(replaceImages)
    {
        [BPImageBO sharedInstance].lastSearch = nil; // invalidate last search to start from zero
    }
    
    [self cancelPreviousSearch];
    [[BPImageBO sharedInstance] loadNewPageForSearch:strSearch completion:^(BOOL hasError){
        self.searchString = [[BPImageBO sharedInstance] lastSearch];
        self.images = [[BPImageBO sharedInstance] searchImages];
        [self.vImageSearch reloadData];
        [self.vImageSearch showActivityIndicator:NO];
        if(replaceImages)
        {
            [self.vImageSearch.cvImages setContentOffset:CGPointZero animated:NO];
            _offsetForInfiniteScroll = 0;
        }
        [self.refreshControl endRefreshing];

    }];
    
    if(showAI)
    {
        [self.vImageSearch showActivityIndicator:[self mustShowActivityIndicator]];
    }
}

-(BOOL) mustShowActivityIndicator
{
    return YES; // todo: know when to show it
}

-(BOOL) hasRefreshControl
{
    return YES;
}

#pragma mark - search delegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    [self loadNewPageForSearch:searchBar.text showActivityIndicator:YES replaceImages:YES];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    self.vImageSearch.sbSearch.showsCancelButton = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    self.vImageSearch.sbSearch.showsCancelButton = NO;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.vImageSearch.sbSearch endEditing:YES];
}

#pragma mark - collection datasource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(![self.images totalPhotos] || [self realCollectionView:self.vImageSearch.cvImages numberOfItemsInSection:0] == 0)
    {
        return 0;
    }
    if([self hasMorePhotosToLoad])
    {
        return [self realCollectionView:collectionView numberOfItemsInSection:section];
    }
    else
    {
        NSUInteger numberOfRealCells = [self realCollectionView:collectionView numberOfItemsInSection:section];
        
        return 3*(numberOfRealCells + kCollectionColums - (numberOfRealCells%kCollectionColums));
    }
}

-(BPImageCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger realRow = [self realRowFromInfiniteScrollRow:indexPath.row];
    
    return [self realCollectionView:collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:realRow inSection:indexPath.section]];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(!decelerate)
    {
        [self scrollViewDidEndDecelerating:scrollView];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(![self hasMorePhotosToLoad])
    {// infinite scroll active
        NSUInteger numberOfRealCells = [self realCollectionView:self.vImageSearch.cvImages numberOfItemsInSection:0];
        NSUInteger numberOfRowsToScroll = (kCollectionColums-1 + numberOfRealCells) / kCollectionColums; // number of rows needed to display all the real cells
        
        CGFloat pageHeight = numberOfRowsToScroll*(kInterRowSpacing + _cellSize);// scroll for all the cells
        
        BOOL firstPage = (scrollView.contentOffset.y >= 0) && (scrollView.contentOffset.y < pageHeight);
        BOOL lastPage = (scrollView.contentOffset.y <= (self.vImageSearch.cvImages.contentSize.height - self.vImageSearch.cvImages.frame.size.height)) && (scrollView.contentOffset.y >= 2*pageHeight);
        
        if(_offsetForInfiniteScroll > 0 && firstPage)
        { // we have scrolled the collection view manually and have to take care of it if we are scrolling up (indexPath.row == 0)
            _offsetForInfiniteScroll -= numberOfRowsToScroll*kCollectionColums;
            [scrollView setContentOffset:CGPointMake(0, scrollView.contentOffset.y + pageHeight) animated:NO];
        }
        else if(lastPage)
        { // we always have to take care about scrolling down
            _offsetForInfiniteScroll += numberOfRowsToScroll*kCollectionColums;
            [scrollView setContentOffset:CGPointMake(0, scrollView.contentOffset.y - pageHeight) animated:NO];
        }
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(_cellSize, _cellSize);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return kInterItemSpacing;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return kInterRowSpacing;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

#pragma mark - real collection view datasource

// real things: as we are simulating an infinite scroll, we name things (cells, etc) that we would have without the infinite scrolling 'real'

-(NSInteger)realCollectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(self.images && [self.images.photo count] > 0)
    {
        NSUInteger numberOfRealCells = [self.images.photo count];
        numberOfRealCells = MAX(numberOfRealCells, [self maximumNumberOfDisplayedCellsInScreen]); // always show a full screen
        return numberOfRealCells;
    }
    return 0;
}

-(BPImageCollectionViewCell *)realCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* cellId = @"flickrImageSearchCell";
    
    BPImageModel* cellImageModel = (BPImageModel*)[self.images.photo objectAtIndex:indexPath.row];
    
    BPImageCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    
    UIImage* placeholder = [UIImage imageNamed:@"Bee1.png"];
    
    [cell.ivImage hnk_setImageFromURL:[cellImageModel imageUrlForSize:FLICKR_SIZE_THUMBNAIL] placeholder:placeholder];
    
    if(indexPath.row == [self realCollectionView:collectionView numberOfItemsInSection:indexPath.section] - 1)
    { // last element
        [self lastImageDidShow];
    }
    
    [cell setIsFavorite:[[BPImageBO sharedInstance] imageIsFavorite:cellImageModel]];
    cell.rowIndex = indexPath.row;
    cell.delegate = self;
    
    return cell;
}

#pragma mark - collection delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger realRow = [self realRowFromInfiniteScrollRow:indexPath.row];
    
    [self realCollectionView:collectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:realRow inSection:indexPath.section]];
}

#pragma mark - real collectionview delegate

-(void)realCollectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BPImageDetailViewController* controller = [[BPImageDetailViewController alloc] init];
    if(controller)
    {
        controller.imageModel = [self.images.photo objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - collection cells delegate

-(void)imageCellDidPressChangeFavoriteAtIndexRow:(NSUInteger)indexRow
{
    BPImageModel* cellImageModel = (BPImageModel*)[self.images.photo objectAtIndex:indexRow];
    
    [[BPImageBO sharedInstance] switchFromFavorites:cellImageModel];
    
    [self.vImageSearch.cvImages reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexRow inSection:0]]];
}

#pragma mark - refresh control

-(void) refreshControlAction
{
    [self loadNewPageForSearch:self.searchString showActivityIndicator:NO replaceImages:YES];
}

@end
