//
//  BPImageSearchViewController.h
//  beePICk
//
//  Created by Sara Victor Fernandez on 1/8/2016.
//  Copyright © 2016 SaraVictor. All rights reserved.
//

#import "BPViewController.h"
#import "BPImageSearchView.h"
#import "BPImageCollectionViewCell.h"

@class BPImageListModel;

@interface BPImageSearchViewController : BPViewController <UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, BPImageCollectionViewCellDelegate>
@property (strong, nonatomic) BPImageListModel* images;
-(BOOL) hasMorePhotosToLoad;
-(BPImageSearchView *)vImageSearch;
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
-(void) loadNewPageForSearch:(NSString*) strSearch showActivityIndicator:(BOOL) showAI replaceImages:(BOOL)replaceImages;
-(void)imageCellDidPressChangeFavoriteAtIndexRow:(NSUInteger)indexRow;
-(BOOL) mustShowActivityIndicator;
-(void) refreshControlAction;
-(BOOL) hasRefreshControl;
@end
