Compilation:

The app was developed and debbuged with Xcode 7.3.1 and OSX El Capitan. To install any dependencies it uses CocoaPods.

Open the project in the included workspace.

Libraries:

Haneke (used to cache and load images asynchronously): A lightweight zero-config image cache for iOS. It resizes images and caches the result on memory and disk. Everything is done in background, allowing for fast, responsive scrolling.